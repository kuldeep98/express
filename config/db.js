import config from 'config'
import mongoose from 'mongoose'

const connectDB = async () => {
  try {
    await mongoose.connect(config.get('mongoURI'), {
      useUnifiedTopology: true,
      useNewUrlParser: true,
      useCreateIndex: true,
    })
    console.log('Database Connected.....')
  } catch (error) {
    console.log('Database Connection Fail!!!')
  }
}

export default connectDB
