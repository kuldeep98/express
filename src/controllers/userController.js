import User from '../models/userModels.js'
import generateToken from '../utils/generateToken.js'
import { loginSchema, registerSchema } from '../validations/userValidator.js'

// @desc    Register a new user
// @route   POST /api/users/register
// @access  Public

const registerUser = async (req, res) => {
  try {
    const { name, email, phone, password } = req.body
    const { errors , value} = await registerSchema.validateAsync({
      name,
      email,
      phone,
      password,
    })
    if (errors) return res.status(406).json({errors})
    const user = await User.create({
      name,
      email,
      phone,
      password,
    })

    if (user) {
      res.status(201).json({ ...user._doc, token: generateToken(user._id) })
    } else {
      res.status(400)
      throw new Error('Invalid user data')
    }
  } catch (error) {
    res.send({ err: error.message })
  }
}

// @desc    Auth user & get token
// @route   POST /api/users/login
// @access  Public

const loginUser = async (req, res) => {
  try {
    const { email, password } = req.body
    const { errors } = await loginSchema.validateAsync({
      email,
      password,
    })
    if (errors) return res.status(406).json({errors})
    const user = await User.findOne({ email })
    if (user && (await user.matchPassword(password)))
      res.status(200).json({
        _id: user._id,
        name: user.name,
        email: user.email,
        phone: user.phone,
        token: generateToken(user._id),
      })
    else {
      res.status(401)
      throw new Error('Invalid email or password')
    }
  } catch (error) {
    res.send({ err: error.message })
  }
}

export { registerUser, loginUser }
