import {Router} from 'express'
import express from 'express'
const router = express.Router()
import userRouter from './userRoutes.js'


router.use('/users',userRouter)

export default router