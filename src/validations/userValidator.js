import joi from 'joi'

const registerSchema = joi.object({
  name: joi
    .string()
    .pattern(/[a-zA-Z]{3,30}/)
    .message('Name is must be greater tha 3 character')
    .required(),
  email: joi.string().email().required(),
  phone: joi.string().pattern(/[0-9]/).min(10).max(10).required(),
  password: joi.string().min(6).alphanum().required(),
})

const loginSchema = joi.object({
  email: joi.string().email().required(),
  password: joi.string().min(6).alphanum().required(),
})
export { registerSchema, loginSchema }
